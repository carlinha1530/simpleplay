package model.dao;

import com.avaje.ebean.Ebean;

import model.vo.Book;
import model.vo.Editora;

public class EditoraDAO extends BaseDAO<Editora> {

	
	public EditoraDAO() {
		super(Editora.class);
	}
	
	public Editora findById(String id) {
		return Ebean.find(Editora.class, id);  
	}
}
