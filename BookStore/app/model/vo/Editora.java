package model.vo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Editora {
	
	@Id
	public String nome;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="editora")
	public List<Book> books;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
	public void addBook(Book b){
		books.add(b);		
	}


	
	
	

}
